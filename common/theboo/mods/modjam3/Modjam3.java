package theboo.mods.modjam3;

import net.minecraftforge.common.Configuration;
import theboo.mods.modjam3.block.ModBlocks;
import theboo.mods.modjam3.config.ConfigurationLoader;
import theboo.mods.modjam3.item.ModItems;
import theboo.mods.modjam3.network.proxy.CommonProxy;
import theboo.mods.modjam3.potion.Potion2;
import theboo.mods.modjam3.util.ModInfo;
import theboo.mods.modjam3.util.TagHelper;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;

@Mod(modid=ModInfo.MOD_ID, name = ModInfo.MOD_NAME, version = ModInfo.VERSION)
public class Modjam3 {
	
	@SidedProxy(clientSide = ModInfo.CLIENT_PROXY_CLASS, serverSide = ModInfo.SERVER_PROXY_CLASS)
	public static CommonProxy proxy;
	
	public static Potion2 tagNumber;
	
	public static Configuration config;
	
	public static ConfigurationLoader loader = new ConfigurationLoader();
	
	@EventHandler
	public static void preInit(FMLPreInitializationEvent fml) {
		config = new Configuration(fml.getSuggestedConfigurationFile());
		loader.loadConfig(config);		
		TagHelper.extendPotionArray();
		tagNumber = new Potion2(30, false, 332432, 0);
		
		ModItems.init();
		ModBlocks.init();
	}
	
	
	@EventHandler()
	public static void init(FMLInitializationEvent fml) {
		
	}
	
	
}
