package theboo.mods.modjam3.config;

import net.minecraftforge.common.Configuration;
import theboo.mods.modjam3.util.BlockInfo;
import theboo.mods.modjam3.util.ItemInfo;


public class ConfigurationLoader {

	public void loadConfig(Configuration c) {
		ItemInfo.TAG_APPLIER_ID = c.getItem("Tag Applier ID", ItemInfo.TAG_APPLIER_ID_DEFAULT).getInt();
		BlockInfo.DETECTOR_ID = c.getBlock("Detector ID", BlockInfo.DETECTOR_ID_DEFAULT).getInt();

	}
	
}
