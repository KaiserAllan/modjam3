package theboo.mods.modjam3.tileentity;

import java.util.List;

import net.minecraft.entity.Entity;
import net.minecraft.tileentity.TileEntity;
import theboo.mods.modjam3.block.BlockTagDetector;
import theboo.mods.modjam3.util.TagHelper;

public class TileEntityTagDetector extends TileEntity {

	public int tag = 0;
	public BlockTagDetector block;
	
	public TileEntityTagDetector(BlockTagDetector block) {
		this.block = block;
	}
	
	public void updateEntity() {
		List list = worldObj.loadedEntityList;
        double d = -1.0D;
        Entity entity = null;

		for(int i=0;i<list.size();++i) {
			Entity entity1 = (Entity)list.get(i);
			if(entity1 == null) return;
			double dist = entity1.getDistanceSq((double) xCoord, (double)yCoord, (double)zCoord);
			
            if ((dist < 6) && (d == -1.0D || dist < d)) {
                d = dist;
                entity = entity1;
            }
		}
		
		if(entity != null) {
			if(TagHelper.isEntityMapped(entity)) {
				int i = TagHelper.getIntFromEntity(entity);
				if(i == tag) {
					block.powering = true;
				} else {
					block.powering = false;
				}
			}
		}
		
	}
	
	public void update(BlockTagDetector block) {
		List list = worldObj.loadedEntityList;
        double d = -1.0D;
        Entity entity = null;

		for(int i=0;i<list.size();++i) {
			Entity entity1 = (Entity)list.get(i);
			if(entity1 == null) return;
			double dist = entity1.getDistanceSq((double) xCoord, (double)yCoord, (double)zCoord);
			
            if ((dist < 6) && (d == -1.0D || dist < d)) {
                d = dist;
                entity = entity1;
            }
		}
		
		if(entity != null) {
			if(TagHelper.isEntityMapped(entity)) {
				int i = TagHelper.getIntFromEntity(entity);
				if(i == tag) {
					block.powering = true;
				} else {
					block.powering = false;
				}
			}
		}
	}
	
	public void changeOperation(boolean sneak) {
		if(sneak) 
			tag--;
		else tag++;
	}

	public int getTag() {
		return tag;
	}
}
