package theboo.mods.modjam3.block;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;
import net.minecraft.block.Block;
import net.minecraft.tileentity.TileEntity;
import theboo.mods.modjam3.tileentity.TileEntityTagDetector;
import theboo.mods.modjam3.util.BlockInfo;

public class ModBlocks {

	public static Block detector;
	
	public static void init() {
		detector = new BlockTagDetector(BlockInfo.DETECTOR_ID).setHardness(15.5F).setStepSound(Block.soundStoneFootstep).setTickRandomly(true).setUnlocalizedName("detector");
		GameRegistry.registerBlock(detector, "detector");
		LanguageRegistry.addName(detector, "Tag Detector");
		GameRegistry.registerTileEntity((Class<? extends TileEntity>) TileEntityTagDetector.class, "tagDetector");
		
	}
	
	
}
