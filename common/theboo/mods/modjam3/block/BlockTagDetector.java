package theboo.mods.modjam3.block;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import theboo.mods.modjam3.tileentity.TileEntityTagDetector;
import theboo.mods.modjam3.util.ModInfo;

public class BlockTagDetector extends Block implements ITileEntityProvider {

	public boolean powering;
	
	public BlockTagDetector(int par1) {
		super(par1, Material.iron);
	}
	
    public int tickRate(World par1World) {
        return 2;
    }
	
	public void registerIcons(IconRegister reg) {
		reg.registerIcon(ModInfo.MOD_ID + ":tagDetector");
	}
	
    public TileEntity createNewTileEntity(World par1World) {
        return new TileEntityTagDetector(this);//brb
    }
    
    public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int par6, float par7, float par8, float par9) {
    	TileEntityTagDetector te = (TileEntityTagDetector)world.getBlockTileEntity(x, y, z);
    	if(te == null) return false;
    	te.changeOperation(player.isSneaking());
    	player.addChatMessage("Operation: if entity has tag: " + te.getTag());
        world.notifyBlocksOfNeighborChange(x, y - 1, z, this.blockID);
        world.notifyBlocksOfNeighborChange(x, y + 1, z, this.blockID);
        world.notifyBlocksOfNeighborChange(x - 1, y, z, this.blockID);
        world.notifyBlocksOfNeighborChange(x + 1, y, z, this.blockID);
        world.notifyBlocksOfNeighborChange(x, y, z - 1, this.blockID);
        world.notifyBlocksOfNeighborChange(x, y, z + 1, this.blockID);
		return true;
    }
    
    public void onBlockAdded(World world, int x, int y, int z) {
        if (world.getBlockMetadata(x, y, z) == 0) {
            super.onBlockAdded(world, x, y, z);
        }

    	if(!powering) return;

        world.notifyBlocksOfNeighborChange(x, y - 1, z, this.blockID);
        world.notifyBlocksOfNeighborChange(x, y + 1, z, this.blockID);
        world.notifyBlocksOfNeighborChange(x - 1, y, z, this.blockID);
        world.notifyBlocksOfNeighborChange(x + 1, y, z, this.blockID);
        world.notifyBlocksOfNeighborChange(x, y, z - 1, this.blockID);
        world.notifyBlocksOfNeighborChange(x, y, z + 1, this.blockID);
    }
    
    public void breakBlock(World par1World, int par2, int par3, int par4, int par5, int par6) {
    	if(!powering) return;
    	
    	par1World.notifyBlocksOfNeighborChange(par2, par3 - 1, par4, this.blockID);
    	par1World.notifyBlocksOfNeighborChange(par2, par3 + 1, par4, this.blockID);
    	par1World.notifyBlocksOfNeighborChange(par2 - 1, par3, par4, this.blockID);
    	par1World.notifyBlocksOfNeighborChange(par2 + 1, par3, par4, this.blockID);
    	par1World.notifyBlocksOfNeighborChange(par2, par3, par4 - 1, this.blockID);
    	par1World.notifyBlocksOfNeighborChange(par2, par3, par4 + 1, this.blockID);
    	
    	super.breakBlock(par1World, par2, par3, par4, par5, par6);
    }
    
    public int isProvidingWeakPower(IBlockAccess par1IBlockAccess, int par2, int par3, int par4, int par5) {
        if (!this.powering)
        {
            return 0;
        }
        else
        {
            return 15;
        }
    }
    
    public int isProvidingStrongPower(IBlockAccess par1IBlockAccess, int par2, int par3, int par4, int par5) {
        return par5 == 0 ? this.isProvidingWeakPower(par1IBlockAccess, par2, par3, par4, par5) : 0;
    }
    
    public boolean canProvidePower() {
        return true;
    }
    
    public void onNeighborBlockChange(World par1World, int par2, int par3, int par4, int par5) {
    	if (this.powering) {
    		par1World.scheduleBlockUpdate(par2, par3, par4, this.blockID, this.tickRate(par1World));
    	}
    }
    
    public void updateTick(World world, int x, int y, int z, Random par5Random) {
    	TileEntityTagDetector te = (TileEntityTagDetector) world.getBlockTileEntity(x, y, z);
    	te.update(this);
    	
		world.scheduleBlockUpdate(x, y, z, this.blockID, this.tickRate(world));
    }
}
