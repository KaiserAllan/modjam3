package theboo.mods.modjam3.potion;

import net.minecraft.potion.Potion;

public class Potion2 extends Potion {

	public int tag;
	
	public Potion2(int par1, boolean par2, int par3, int tag) {
		super(par1, par2, par3);
		this.tag = tag;
	}
	
	public Potion setIconIndex(int par1, int par2) {
		super.setIconIndex(par1, par2);
		return this;
	}
	
	
	
}
