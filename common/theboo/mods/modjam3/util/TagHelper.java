package theboo.mods.modjam3.util;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

import net.minecraft.entity.Entity;
import net.minecraft.potion.Potion;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

public class TagHelper {

	private static HashBiMap<Integer, Entity> tagMap = HashBiMap.create();
	
	public static void addMapping(int i, Entity e) {
		if(tagMap.containsKey(i) || tagMap.inverse().containsKey(e)) return;
		
		tagMap.put(i, e);
	}
	
	public static boolean isEntityMapped(Entity e) {
		return (tagMap.containsValue(e));
	}
	
	public static Entity getEntityFromInt(int i) {
		if(tagMap.containsKey(i)) 
			return tagMap.get(i);
		return null;
 	}
	
	public static int getIntFromEntity(Entity e) {
		BiMap<Entity, Integer> bimap = tagMap.inverse();
		
		if(bimap.containsKey(e)) 
			return bimap.get(e);
		return 0;
 	}
	
	public static void extendPotionArray() {
		Potion[] potionTypes = null;

		for (Field f : Potion.class.getDeclaredFields()) {
			f.setAccessible(true);
			try {
				if (f.getName().equals("potionTypes") || f.getName().equals("field_76425_a")) {
					Field modfield = Field.class.getDeclaredField("modifiers");
					modfield.setAccessible(true);
					modfield.setInt(f, f.getModifiers() & ~Modifier.FINAL);

					potionTypes = (Potion[])f.get(null);
					final Potion[] newPotionTypes = new Potion[256];
					System.arraycopy(potionTypes, 0, newPotionTypes, 0, potionTypes.length);
					f.set(null, newPotionTypes);
				}
			}
			catch (Exception e) {
				System.err.println("Severe error occured; could not extend field_76425_a:");
				System.err.println(e);
			}
		}
	}
}
