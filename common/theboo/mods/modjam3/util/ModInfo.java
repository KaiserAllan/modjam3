package theboo.mods.modjam3.util;

public class ModInfo {

	public static final String MOD_NAME = "modjam3";
	public static final String MOD_ID = "Modjam3";
	public static final String VERSION = "1.0";
	public static final String CLIENT_PROXY_CLASS = "theboo.mods.modjam3.network.proxy.ClientProxy";
	public static final String SERVER_PROXY_CLASS = "theboo.mods.modjam3.network.proxy.CommonProxy";
	
}
