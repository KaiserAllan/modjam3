package theboo.mods.modjam3.item;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import theboo.mods.modjam3.Modjam3;
import theboo.mods.modjam3.util.ModInfo;
import theboo.mods.modjam3.util.TagHelper;

public class ItemTagApplier extends Item {

	int currentTag;
	
	boolean operate1 = true;
	boolean operate2 = true; 
	int mode = 0;
	
	public ItemTagApplier(int id) {
		super(id);
		currentTag = 0;
	}
	
    public ItemStack onItemRightClick(ItemStack stack, World par2World, EntityPlayer player) {    	
    	if(operate1 == true) 
    		operate1 = false;
    	else {
    		operate1 = true;
    		return stack;
    	}
    	
    	if(player.isSneaking()) 
    		currentTag--;    	
    	else 
    		currentTag++;
    	
    	player.addChatMessage("Current Tag ID: " + currentTag);
    	
        return stack;
    }
	
    public boolean itemInteractionForEntity(ItemStack par1ItemStack, EntityPlayer player, EntityLivingBase entity) {
    	if(mode == 0) {
        	if(operate2 == true) 
        		operate2 = false;
        	else {
        		operate2 = true;
        		return false;
        	}
        	
            if(entity.getActivePotionEffect(Modjam3.tagNumber) == null && entity != null) {
            	TagHelper.addMapping(currentTag, entity);    
            	player.addChatMessage("Tag: " + entity + " : " + currentTag);
            }
    	} else if(mode == 1) {
            if(entity.getActivePotionEffect(Modjam3.tagNumber) == null && entity != null) {
            	player.addChatMessage(": " + TagHelper.getIntFromEntity(entity));    

            }
    	}

    	
    	return true;
    }
    
    public boolean onItemUse(ItemStack par1ItemStack, EntityPlayer player, World par3World, int par4, int par5, int par6, int par7, float par8, float par9, float par10) {
        return false;
    }
	
    public void registerIcons(IconRegister reg) {
		itemIcon = reg.registerIcon(ModInfo.MOD_ID + ":tagger");
    }
}
