package theboo.mods.modjam3.item;

import cpw.mods.fml.common.registry.LanguageRegistry;
import theboo.mods.modjam3.util.ItemInfo;
import net.minecraft.item.Item;

public class ModItems {

	public static Item tagApplier;
	
	public static void init() {
		tagApplier = new ItemTagApplier(ItemInfo.TAG_APPLIER_ID).setUnlocalizedName("tagApplier");
		
		LanguageRegistry.addName(tagApplier, "Tag Applier");
		
	}
}
